## Templates with configMaps

#### Create the configmap

```
apiVersion: v1
data:
  maxmemory: 2mb
  maxmemory-policy: allkeys-lru
kind: ConfigMap
metadata:
  name: <application>-app-config
```
Replace the two lines containing `maxmemory` with your configuration(s) and save the file in your `.openshift-applier/files/<filename>.yml` directory.
Also replace `<application>` with the name of your application to avoid overwriting other application's data.

#### Apply the configmap with the Applier

```
openshift_cluster_content:
...
- object: configmap
  content:
  - name: "{{ app_name }}-configmap"
    file: "{{ inventory_dir }}/../files/<filename>.yml"
    tags:
      - "{{ dev_namespace }}"
      - "{{ dev_namespace }}-configmap"
...
```

#### Profit
The configmap data is now available as environment variables inside the container.
```
oc rsh <pod> env | grep maxmemory
maxmemory=2mb
maxmemory-policy=allkeys-lru
```
