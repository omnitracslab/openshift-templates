---
kind: Template
apiVersion: v1
metadata:
  name: http-app-deploy
  annotations:
    openshift.io/display-name: HTTP App Deploy Template
    description: A template to deploy your an App with a HTTP endpoint
    iconClass: icon-cube
    tags: http
objects:
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      build: "${NAME}"
    name: "${NAME}"
  spec: {}
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: "${NAME}"
    labels:
      app: "${NAME}"
  spec:
    replicas: 1
    selector:
      name: "${NAME}"
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        labels:
          name: "${NAME}"
      spec:
        initContainers:
          - resources:
              limits:
                cpu: 100m
                memory: 100Mi
            terminationMessagePath: /dev/termination-log
            name: convert-cert
            command:
              - /bin/bash
            imagePullPolicy: IfNotPresent
            volumeMounts:
              - name: ca-anchors
                mountPath: /usr/share/pki/ca-trust-source/anchors
              - name: ca-extracted
                mountPath: /etc/pki/ca-trust/extracted
            terminationMessagePolicy: File
            image: 'rhel7/support-tools:7.6'
            args:
              - '-c'
              - >-
                cp
                /var/run/secrets/kubernetes.io/serviceaccount/service-ca.crt
                /usr/share/pki/ca-trust-source/anchors && mkdir -p
                /etc/pki/ca-trust/extracted/pem
                /etc/pki/ca-trust/extracted/openssl
                /etc/pki/ca-trust/extracted/java && update-ca-trust
        containers:
        - envFrom:
            - configMapRef:
                name: "${NAME}-app-config"
          image: "${NAME}"
          imagePullPolicy: Always
          name: "${NAME}"
          ports:
          - containerPort: 8778
            protocol: TCP
          - containerPort: 8080
            protocol: TCP
          - containerPort: 8443
            protocol: TCP
          resources:
            limits:
              cpu: "${CONTAINER_CPU_LIMIT_SIZE}"
              memory: "${CONTAINER_MEMORY_LIMIT_SIZE}"
            requests:
              cpu: "${CONTAINER_CPU_REQUEST_SIZE}"
              memory: "${CONTAINER_MEMORY_REQUEST_SIZE}"
          terminationMessagePath: "/dev/termination-log"
          volumeMounts:
            - name: ca-anchors
              mountPath: /usr/share/pki/ca-trust-source/anchors
            - name: ca-extracted
              mountPath: /etc/pki/ca-trust/extracted
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        securityContext: {}
        terminationGracePeriodSeconds: 30
        serviceAccountName: "${SERVICE_ACCOUNT_NAME}"
        serviceAccount: "${SERVICE_ACCOUNT_NAME}"
        volumes:
          - name: ca-anchors
            emptyDir: {}
          - name: ca-extracted
            emptyDir: {}
    test: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - "${NAME}"
        from:
          kind: ImageStreamTag
          name: "${NAME}:${DEPLOY_IMAGE_STREAM_TAG_NAME}"
      type: ImageChange
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      name: "${NAME}"
    name: "${NAME}"
  spec:
    ports:
    - name: 8080-tcp
      port: 8080
      protocol: TCP
      targetPort: 8080
    - name: 8443-tcp
      port: 8443
      protocol: TCP
      targetPort: 8443
    - name: 8778-tcp
      port: 8778
      protocol: TCP
      targetPort: 8778
    selector:
      name: "${NAME}"
    sessionAffinity: None
    type: ClusterIP
- apiVersion: v1
  kind: Route
  metadata:
    labels:
      name: "${NAME}"
    name: "${NAME}"
  spec:
    port:
      targetPort: 8080-tcp
    tls:
      termination: edge
    to:
      kind: Service
      name: "${NAME}"
      weight: 100
    wildcardPolicy: None
parameters:
- name: NAME
  displayName: Name
  description: The name assigned to all objects and the related imagestream.
  required: true
- name: DEPLOY_IMAGE_STREAM_TAG_NAME
  displayName: The ImageStreamTag name to use for deployment
  description: The ImageStreamTag name to use for deployment
  required: true
  value: deployed
- name: CONTAINER_CPU_REQUEST_SIZE
  displayName: The minimum amount of CPU the container is guaranteed.
  description: The minimum amount of CPU the container is guaranteed.
  required: true
  value: 100m
- name: CONTAINER_CPU_LIMIT_SIZE
  displayName: The maximum amount of CPU the container is allowed to use when running.
  description: The maximum amount of CPU the container is allowed to use when running.
  required: true
  value: "500m"
- name: CONTAINER_MEMORY_REQUEST_SIZE
  displayName: The minimum amount of memory the container is guaranteed.
  description: The minimum amount of memory the container is guaranteed.
  required: true
  value: 256Mi
- name: CONTAINER_MEMORY_LIMIT_SIZE
  displayName: The maximum amount of memory the container is allowed to use when running.
  description: The maximum amount of memory the container is allowed to use when running.
  required: true
  value: 512Mi
- name: SERVICE_ACCOUNT_NAME
  displayName: The ServiceAccount name to use for deployment
  description: The ServiceAccount name to use for deployment
  required: true
  value: default
labels:
  template: http-app-deploy-template
