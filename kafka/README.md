# Ephemeral Kafka cluster template
This is used to create a kafka cluster on a previously installed Operator in a namespace.
The AOC team should be able to help with installing the operator in your required namespace proir to applying the template.

#### Misc notes
Usually you want an odd number of zookeeper nodes.
Its possible to run a single zookeeper and kafka node (the default within this template) but 3 is usually the minimum for each.
The offsets.topic.replication.factor should match or be smaller than the kafka node size.

* [Zookeeper replicas](https://strimzi.io/docs/master/#assembly-zookeeper-replicas-deployment-configuration-kafka)
* [Kafka broker replicas](https://strimzi.io/docs/master/#assembly-kafka-broker-replicas-deployment-configuration-kafka)

Changes to the Kafka type object in openshift should get implemented by the running operator after a period of time. Sometimes the operator needs to be restarted to force the changes to be picked up.

#### Examples:
~~~
[tbox@localhost kafka]$ oc process -f kafka-ephemeral-CR-template.yml -p NAMESPACE=platform-dev -p ZOOKEEPER_NODE_COUNT=1 -p CLUSTER_NAME=platform-kafka-cluster | oc apply -n platform-dev -f -
kafka.kafka.strimzi.io/platform-kafka-cluster configured
[tbox@localhost kafka]$ oc process -f kafka-ephemeral-CR-template.yml -p NAMESPACE=platform-test -p ZOOKEEPER_NODE_COUNT=1 -p CLUSTER_NAME=platform-kafka-cluster | oc apply -n platform-test -f -
kafka.kafka.strimzi.io/platform-kafka-cluster configured
~~~
