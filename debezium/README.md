# Notes

debezium-init.json is an example json file you may start with.

You can import the contents of the file into a variable / parameter during your playbook execution using lookup:

`INIT_JSON: "{{ lookup('file', '{{ playbook_dir }}/files/debezium-init.json') | from_json | to_json }}"`

Environment variables in the json file will be replaced at runtime by the postStart.sh script in the configmap
