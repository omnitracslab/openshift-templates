apiVersion: v1
kind: Template
metadata:
  name: prometheus
labels:
  app: prometheus
objects:
- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    labels:
      app: prometheus
    name: prometheus
  spec:
    replicas: 1
    revisionHistoryLimit: 10
    selector:
      app: prometheus
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        creationTimestamp: null
        labels:
          app: prometheus
      spec:
        containers:
        - image: quay.io/prometheus/prometheus@sha256:789c81b82b2a899bfb88ef970d94c386710a8b23140a008ac284d2f059ec4fd5
          imagePullPolicy: IfNotPresent
          name: prometheus
          ports:
          - containerPort: 9090
            protocol: TCP
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
          - mountPath: /prometheus
            name: prometheus
          - mountPath: /etc/prometheus
            name: prometheus-config
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
        - name: prometheus
          persistentVolumeClaim:
            claimName: prometheus
        - configMap:
            defaultMode: 420
            name: prometheus-config
          name: prometheus-config
    test: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - prometheus
        from:
          kind: ImageStreamTag
          name: prometheus:latest
      type: ImageChange
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: prometheus
    name: prometheus
  spec:
    ports:
    - port: 9090
      protocol: TCP
      targetPort: 9090
    selector:
      app: prometheus
    sessionAffinity: None
    type: ClusterIP
  status:
    loadBalancer: {}
- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    labels:
      app: prometheus
    name: prometheus
  spec:
    port:
      targetPort: 9090
    subdomain: ""
    to:
      kind: Service
      name: prometheus
      weight: 100
    wildcardPolicy: None
- apiVersion: v1
  data:
    prometheus.yml: |-
      # my global config
      global:
        scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
        evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
        # scrape_timeout is set to the global default (10s).

      # Alertmanager configuration
      alerting:
        alertmanagers:
        - static_configs:
          - targets:
            # - alertmanager:9093

      # Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
      rule_files:
        # - "first_rules.yml"
        # - "second_rules.yml"


      scrape_configs:
      - job_name: 'kubernetes-pods'
        kubernetes_sd_configs:
        - role: pod
          namespaces:
            names:
            - ${NAMESPACE}

        relabel_configs:
        - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_scrape]
          action: keep
          regex: true
        - source_labels: [__meta_kubernetes_pod_annotation_prometheus_io_path]
          action: replace
          target_label: __metrics_path__
          regex: (.+)
        - source_labels: [__address__, __meta_kubernetes_pod_annotation_prometheus_io_port]
          action: replace
          regex: ([^:]+)(?::\d+)?;(\d+)
          replacement: $1:$2
          target_label: __address__
        - action: labelmap
          regex: __meta_kubernetes_pod_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_pod_name]
          action: replace
          target_label: kubernetes_pod_name
  kind: ConfigMap
  metadata:
    name: prometheus-config
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    finalizers:
    - kubernetes.io/pvc-protection
    name: prometheus
  spec:
    accessModes:
    - ReadWriteOnce
    dataSource: null
    resources:
      requests:
        storage: 4Gi
    storageClassName: gp2
    volumeMode: Filesystem
  status: {}
- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    annotations:
      openshift.io/image.dockerRepositoryCheck: 2019-06-24T16:06:45Z
    creationTimestamp: null
    generation: 1
    labels:
      app: prometheus
    name: prometheus
  spec:
    lookupPolicy:
      local: false
    tags:
    - annotations:
        openshift.io/generated-by: OpenShiftWebConsole
        openshift.io/imported-from: quay.io/prometheus/prometheus
      from:
        kind: DockerImage
        name: quay.io/prometheus/prometheus
      generation: 1
      importPolicy: {}
      name: latest
      referencePolicy:
        type: Source
  status:
    dockerImageRepository: image-registry.openshift-image-registry.svc:5000/prometheus
- apiVersion: rbac.authorization.k8s.io/v1
  kind: RoleBinding
  metadata:
    name: view
    namespace: ${NAMESPACE}
  roleRef:
    apiGroup: rbac.authorization.k8s.io
    kind: ClusterRole
    name: view
  subjects:
  - kind: ServiceAccount
    name: default
    namespace: ${NAMESPACE}
parameters:
  - name: NAMESPACE
    description: The namespace
    required: true